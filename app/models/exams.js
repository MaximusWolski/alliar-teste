const Sequelize = require('sequelize');
const db = require('../util/database');

const Exams = db.define('exams',{
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    type_exam: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: false
    },
    status: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
});

module.exports = Exams;