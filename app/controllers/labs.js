const Labs = require('../models/labs');

exports.getAll = async (req, res, next) => {
  try {
    const ALL = await Labs.findAll();
    return res.status(200).json(ALL);
  } catch (error) {
    return res.status(500).json(error);
  }
};

exports.getOne = async (req, res, next) => {
  try {
    const lab = await Labs.findByPk(req.params.id);
    return res.status(200).json(lab);
  } catch (error) {
    return res.status(500).json(error);
  }
};

exports.createOne = async (req, res, next) => {
  try {
    const LABS_MODEL = {
      name: req.body.name,
      address: req.body.address,
      status: req.body.status,
    };

    try {
      const lab = await Labs.create(LABS_MODEL);
      console.log('Lab created!');
      return res.status(201).json(lab);
    } catch (error) {
      return res.status(500).json(error);
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

exports.updateOne = async (req, res, next) => {
  try {
    const LABS_MODEL = {
      name: req.body.name,
      address: req.body.address,
      status: req.body.status,
    };

    try {
      const lab = await Labs.update(LABS_MODEL, { where: { id: req.params.id } });
      return res.status(200).json(lab);
    } catch (error) {}
  } catch (error) {
    return res.status(500).json(error);
  }
};

exports.deleteOne = async (req, res, next) => {
  try {
    const lab = await Labs.destroy({ where: { id: req.params.id } });
    return res.status(200).json(lab);
  } catch (error) {
    return res.status(500).json(error);
  }
};
