const Exams = require('../models/exams');

exports.getAll = async (req, res, next) => {
  try {
    const ALL = await Exams.findAll();
    return res.status(200).json(ALL);
  } catch (error) {
    return res.status(500).json(error);
  }
};

exports.getOne = async (req, res, next) => {
  try {
    const Exams = await Exams.findByPk(req.params.id);
    return res.status(200).json(exams);
  } catch (error) {
    return res.status(500).json(error);
  }
};

exports.createOne = async (req, res, next) => {
  try {
    const EXAMS_MODEL = {
      type_exam: req.body.type_exam,
      status: req.body.status,
    };

    try {
      const user = await Exams.create(EXAMS_MODEL);
      console.log('Exam created!');
      return res.status(201).json(exams);
    } catch (error) {
      return res.status(500).json(error);
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

exports.updateOne = async (req, res, next) => {
  try {
    const EXAMS_MODEL = {
      type_exam: req.body.type_exam,
      status: req.body.status,
    };

    try {
      const user = await Exams.update(EXAMS_MODEL, { where: { id: req.params.id } });
      return res.status(200).json(exams);
    } catch (error) {}
  } catch (error) {
    return res.status(500).json(error);
  }
};

exports.deleteOne = async (req, res, next) => {
  try {
    const labs = await Exams.destroy({ where: { id: req.params.id } });
    return res.status(200).json(exams);
  } catch (error) {
    return res.status(500).json(error);
  }
};
