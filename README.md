



##Para rodar o Projeto.

#1. Após baixar o git, rode o comando:<b>docker-compose up --build
#2. em seu Postman utilize os seguintes endpoints:
   Lista Laboratórios:
   http://localhost:3001/labs - Verbo GET (lista todos os laboratorios cadastrados);

##Para incluir um novo laboratorio:
http://localhost:3001/labs - Verbo Post
Marque a opção: body >> raw
e preencha os campos conforme o modelo abaixo:

{
"name": "nome do laboratorio",
"address": "endereço do laboratorio",
"status": "true"
}

##Para Selecionar um Laboratório:
http://localhost/labs/<id> - Verbo Get
listará os dados do laboratorio selecionado

##Para editar um laboratorio:
http://localhost:3001/labs/<id> - Verbo Put
{
"name":"Blood Vampires",
"address":"Av. Paulista, 1541",
"status":"false"
}

##Para deletar um laboratorio:
http://localhost:3001/<id> - Verbo Delete



##Para incluir um novo Exame:
http://localhost:3001/exams - Verbo Post
Marque a opção: body >> raw
e preencha os campos conforme o modelo abaixo:
{
"type_exam": "Análises Clínicas",
"status": "true"
}

##Para Selecionar um Exame:
http://localhost/exams/<id> - Verbo Get
listará os dados do laboratorio selecionado

##Para editar um laboratorio:
http://localhost:3001/exams/<id> - Verbo Put
{
"type_exam": "Análises Clínicas",
"status": "true"
}

##Para deletar um laboratorio:
http://localhost:3001/exams/<id> - Verbo Delete


##OBSERVAÇÃO:
Houve um "ruido" durante a semana que me impossibilitou de finalizar o projeto,
pois recebi a mensagem abaixo:

"Agradecemos a sua participação no processo seletivo para vaga de Dev. NodeJS(Cliente Alliar).
Gostaríamos de informá-lo que, infelizmente, você não foi selecionado para próxima etapa do 
processo seletivo da oportunidade em questão.

Obrigada por sua participação e espero convidá-lo a participar de futuros processos na Moot.

Desejamos sucesso na sua trajetória profissional.

Atenciosamente,"

Assim sendo eu NAO FINALIZEI o projeto

Pois estava finalizando os enndpoints referentes a exames, que iam gerenciar justamente os casos de
quais exames estariam em quais laboratorios conforme a regra informada no arquivo backend.md
por conta da mensagem acima eu abortei a missão, e o teste não foi finalizado.

Acredito que com isso voces consigam analisar a lógica usada no projeto.

Porém caso seja imprescindível, poderei finalizar, fico no aguardo do feed back
